# Rename this file "config.py" and assign the empty values

from datetime import timedelta

# Flask secret key
SECRET_KEY = ""

# MySQL variables
MYSQL_DATABASE_HOST = ""
MYSQL_DATABASE_USER = ""
MYSQL_DATABASE_PASSWORD = ""
MYSQL_DATABASE_DB = ""

# Flask-Mail variables
MAIL_SERVER = ""
MAIL_PORT = 587
MAIL_USE_TLS = True
MAIL_USE_SSL = True
MAIL_USERNAME = ""
MAIL_PASSWORD = ""
MAIL_DEFAULT_SENDER = ""

# JWT config
JWT_ACCESS_TOKEN_EXPIRES = timedelta(weeks=4)
JWT_SECRET_KEY = ""
JWT_TOKEN_LOCATION = ['cookies']
JWT_ACCESS_COOKIE_PATH = '/'
JWT_COOKIE_CSRF_PROTECT = True
JWT_COOKIE_SECURE = True