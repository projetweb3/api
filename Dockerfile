FROM python

COPY requirements.txt /tmp/requirements.txt

RUN pip install -r /tmp/requirements.txt

RUN apt update && apt upgrade -y

WORKDIR /app

RUN mkdir app instance

COPY app ./app

COPY instance/ ./instance

COPY .flaskenv .

RUN groupadd -r app &&\
    useradd -r -g app -d /app -s /sbin/nologin -c "Docker image user" app &&\
    chown -R app:app /app

USER app

ENTRYPOINT ["gunicorn"]

CMD ["-w", "4", "--bind", "0.0.0.0:5000", "app:create_app()"]