from flask import Blueprint
from flask import jsonify
from flask import request
from flask_jwt_extended import jwt_optional
from flask_jwt_extended import get_jwt_identity
from flask_jwt_extended import create_access_token, set_access_cookies, jwt_required, unset_jwt_cookies

from app.extensions import db
from app.extensions import bcrypt
from app.extensions import jwt

bp = Blueprint('auth', __name__, url_prefix='/auth')

@bp.route('/authenticated', methods=['GET'])
@jwt_optional
def is_authenticated():
    user = get_jwt_identity()
    if user:
        return jsonify({
            'first_name': user['first_name'],
            'last_name': user['last_name'],
            'email': user['email'],
            'status': user['status_id']
        }), 200
    else:
        return jsonify({
            'authenticated': False
        }), 200

@bp.route('/signup', methods=['POST'])
def signup():
    data = request.get_json()
    cursor = db.get_db().cursor()
    cursor.callproc('user_getByEmail', [data['email']])
    email_already_in_use = cursor.fetchone()

    errors = list()    
    for user_input in data:
        if not data[user_input]:
            errors.append('Vous devez remplir tous les champs.')
            break

    if email_already_in_use is not None:
        errors.append("Cet email est déjà utilisé.")
    if data['password'] != data['repeat-password']:
        errors.append('Les mots de passe ne sont pas identiques.')

    if errors:
        return jsonify({
            'message': ''.join(errors)
        }), 400

    hashed_password = bcrypt.generate_password_hash(data['password']).decode('utf-8')
    cursor.callproc('user_create', [data['first_name'], data['last_name'], data['email'], hashed_password])    
    db.get_db().commit()
    
    return jsonify({
        'message': 'Compte créé'
        }), 201


# Method to define what information will be stored in the JWT
@jwt.user_claims_loader
def add_claims_to_access_token(identity):
    return {
        'id': identity['id'],
        'last_name': identity['last_name'],
        'first_name': identity['first_name'],
        'email': identity['email'],
        'status_id': identity['status_id']
    }

@bp.route('/login', methods=['POST'])
def login():
    data = request.get_json()

    email = data['email']
    password = data['password']

    cursor = db.get_db().cursor()
    cursor.callproc('user_getByEmail', [email])
    user = cursor.fetchone()
    
    errors = list()
    if user is None:
        errors.append('Cet email n\'appartient à aucun compte.')
    elif not bcrypt.check_password_hash(user['password'], password):
        errors.append('Mauvais mot de passe.')

    if errors:
        return jsonify({
            'message': ''.join(errors)
        }), 401

    jwt_data = {
        'id': user['id'],
        'first_name': user['first_name'],
        'last_name': user['last_name'],
        'email': user['email'],
        'status_id': user['status_id'],
    }
    access_token = create_access_token(jwt_data)
    resp = jsonify({'Login': 'Success'})
    set_access_cookies(resp, access_token)
    
    return resp, 200

@bp.route('/logout', methods=['GET'])
@jwt_required
def logout():
    resp = jsonify({
        'Logout': 'Success'
    })
    
    unset_jwt_cookies(resp)
    return resp, 200

