from flask import Flask

from app import auth
from app import user
from app import contact
from app import document

from app.extensions import bcrypt
from app.extensions import db
from app.extensions import mail
from app.extensions import jwt

def create_app():
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_pyfile("config.py", silent=True)

    # Initialize extensions
    bcrypt.init_app(app)
    db.init_app(app)
    mail.init_app(app)
    jwt.init_app(app)

    # Register blueprints
    app.register_blueprint(auth.bp)
    app.register_blueprint(user.bp)
    app.register_blueprint(contact.bp)
    app.register_blueprint(document.bp)

    # Root route
    @app.route('/')
    def _root_route():
        return "Welcome to MyBroker API !"

    return app
    