import hashlib
import time

from flask_mail import Message

from app.extensions import mail

def create_filename(user):
    timestamp = time.time()
    
    hash = hashlib.sha1()
    hash.update(str(timestamp).encode('utf-8') + user['email'].encode('utf-8'))

    return hash.hexdigest()

def send_mail(subject, body, to, reply_to=None):
    if reply_to:
        msg = Message(subject, recipients=[to], body=body, reply_to=reply_to)
    else:
        msg = Message(subject, recipients=[to], body=body)
    
    mail.send(msg)
