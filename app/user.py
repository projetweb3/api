from flask import Blueprint
from flask import jsonify, request
from flask_jwt_extended import jwt_required, get_jwt_identity, unset_jwt_cookies

from app.extensions import db
from app.extensions import bcrypt

bp = Blueprint('user', __name__, url_prefix='/user')

@bp.route('/', methods=['GET'])
@jwt_required
def user():
    user = get_jwt_identity()
    cursor = db.get_db().cursor()
    if user['status_id'] == 1:
        cursor.callproc('user_get')
    else:
        cursor.callproc('user_getByEmail', [user['email']])

    user = cursor.fetchall()

    return jsonify(
        user
    ), 200

@bp.route('/status', methods=['GET'])
@jwt_required
def user_status():
    user = get_jwt_identity()
    
    cursor = db.get_db().cursor()
    cursor.callproc('user_getStatusNameFrById', [user['id']])

    status = cursor.fetchone()

    return jsonify({
        'status': status['name_fr']
    }), 200

@bp.route('/email', methods=['PATCH'])
@jwt_required
def user_email():
    new_mail = request.get_json()['email']
    user_id = get_jwt_identity()['id']

    cursor = db.get_db().cursor()
    cursor.callproc('user_getByEmail', [new_mail])
    email_already_in_use = cursor.fetchone()

    errors = list()    
    if not new_mail:
        errors.append('Vous devez remplir tous les champs.')

    if email_already_in_use is not None:
        errors.append("Cet email est déjà utilisé.")

    if errors:
        return jsonify({
            'message': ''.join(errors)
        }), 400
    
    cursor = db.get_db().cursor()
    cursor.callproc('user_updateEmailById', [user_id, new_mail])
    db.get_db().commit()
    resp = jsonify({
        'EmailChange': 'Success'
    })
    unset_jwt_cookies(resp)
    return resp, 200

@bp.route('/password', methods=['PATCH'])
@jwt_required
def user_password():
    data = request.get_json()
    user_id = get_jwt_identity()['id']

    errors = list()    
    for user_input in data:
        if not data[user_input]:
            errors.append('Vous devez remplir tous les champs.')
            break

    if data['password'] != data['repeat-password']:
        errors.append('Les mots de passe ne sont pas identiques.')

    if errors:
        return jsonify({
            'message': ''.join(errors)
        }), 400

    new_password = bcrypt.generate_password_hash(data['password']).decode('utf-8')
    
    cursor = db.get_db().cursor()
    cursor.callproc('user_updatePasswordById', [user_id, new_password])
    db.get_db().commit()
    
    resp = jsonify({
        'passwordChange': 'Success'
    })
    
    unset_jwt_cookies(resp)
    return resp, 200
