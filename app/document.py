from flask import Blueprint
from flask import jsonify, request, send_from_directory
from flask_jwt_extended import jwt_required, get_jwt_identity, unset_jwt_cookies

from app.extensions import db
from app.extensions import bcrypt
from app.utils import send_mail
from app.utils import create_filename

import base64

bp = Blueprint('document', __name__)

@bp.route('/document/', methods=['GET', 'POST', 'DELETE'])
@jwt_required
def document():
    user = get_jwt_identity()
    
    if request.method == 'GET':
        cursor = db.get_db().cursor()
        
        if user['status_id'] == 1:
            cursor.callproc('document_get')
        else:
            cursor.callproc('document_getByUserId', [user['id']])
        documents = cursor.fetchall()

        if cursor.rowcount == 0:
            return jsonify([{
                'id': '',
                'path': '',
                'user_id': '',
                'creation_date': ''
            }]), 200

        return jsonify(
            documents
        ), 200
    elif request.method == 'POST':
        data = request.get_json()

        errors = list()    
        for user_input in data:
            if user_input == 'user_id':
                pass
            elif not data[user_input]:
                errors.append('Vous devez remplir tous les champs.')
                break

        if errors:
            return jsonify({
                'message': ''.join(errors)
            }), 400

        filename = create_filename(user) + '.' + data['docExtension']

        with open('app/documents/' + filename, 'wb') as fh:
            fh.write(base64.decodebytes(str.encode(data['doc'].split(',')[1])))

        cursor = db.get_db().cursor()

        if user['status_id'] == 1:
            cursor.callproc('document_create', [filename, data['name'], data['user_id']])
        else:
            cursor.callproc('document_create', [filename, data['name'], user['id']])
        
        db.get_db().commit()

        return jsonify({
            'upload': 'successful'
        }), 200

    elif request.method == 'DELETE':
        id = request.get_json()['id']

        cursor = db.get_db().cursor()
        cursor.callproc('document_deleteById', [id])
        db.get_db().commit()

        return jsonify({
            'delete': 'success'
        }), 200

@bp.route('/document/<path>')
def send_document(path):
    return send_from_directory('documents', path)