from flask_bcrypt import Bcrypt
from flask_mail import Mail
from flaskext.mysql import MySQL
from pymysql.cursors import DictCursor
from flask_jwt_extended import JWTManager

db = MySQL(cursorclass=DictCursor)
bcrypt = Bcrypt()
mail = Mail()
jwt = JWTManager()