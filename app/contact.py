from flask import Blueprint
from flask import jsonify, request
from flask_jwt_extended import jwt_required, get_jwt_identity, unset_jwt_cookies

from app.extensions import db
from app.extensions import bcrypt
from app.utils import send_mail

bp = Blueprint('contact', __name__)

@bp.route('/contact', methods=['POST'])
def contact():
    data = request.get_json()

    errors = list()    
    for user_input in data:
        if not data[user_input]:
            errors.append('Vous devez remplir tous les champs.')
            break

    if errors:
        return jsonify({
            'message': ''.join(errors)
        }), 400

    send_mail('MyBroker : ' + data['subject'], data['message'], 'ameliecourtin3@gmail.com', data['email'])

    return jsonify({
        'contact': 'success'
    }), 200